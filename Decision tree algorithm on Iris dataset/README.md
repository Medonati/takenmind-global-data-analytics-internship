﻿**Project Overview**

*  Create a DataFrame by importing a new IRIS.csv file created using the data from the following link: https://www.kaggle.com/uciml/iris
 
(NOTE:  IRIS dataset is a very standard dataset in the field of data science. The dataset includes three iris species with 50 samples each as well as some properties about each flower. The available columns in this dataset are: 
Id, SepalLengthCm, SepalWidthCm, PetalLengthCm, PetalWidthCm, and Species)
 
3: Split the IRIS dataset into 2 subsets: Training Dataset (70% rows) and Testing Dataset (30% rows)
 
4: Implement the decision tree classifier algorithm on the IRIS Training dataset. Test the accuracy with Testing Dataset. You are allowed to use any ready-made library for decision tree if needed.